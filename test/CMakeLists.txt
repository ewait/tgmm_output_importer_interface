
cmake_minimum_required ( VERSION 2.8 )

#add flags for the C++11 
if(CMAKE_COMPILER_IS_GNUCXX) 
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
elseif ("${CMAKE_CXX_COMPILER_ID}" MATCHES "Clang")
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -stdlib=libc++ -std=c++11")
endif()

include_directories(../src/)

IF(NOT TARGET test_TGMM_parser_XML )
	include_directories(../src/parse_XML_output)
	add_executable(test_TGMM_parser_XML test_TGMM_parser_XML.cpp)
	target_link_libraries(test_TGMM_parser_XML TGMM_parser_lib)
ENDIF()


IF(NOT TARGET test_TGMM_parser_SQL )

	#to locate scripts
	set (CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/../src/parse_SQL_CATMAID/cmake") 

	#check if have PostGre SQL library
	FIND_PACKAGE(PostgreSQL)

	if( PostgreSQL_FOUND )
		include_directories(../src/parse_SQL_CATMAID)
		include_directories(${PostgreSQL_INCLUDE_DIRS})		
		add_executable(test_TGMM_parser_SQL test_TGMM_parser_SQL.cpp)
		target_link_libraries(test_TGMM_parser_SQL TGMM_parser_lib)
		TARGET_LINK_LIBRARIES(test_TGMM_parser_SQL ${PostgreSQL_LIBRARY})		
	else()
		message("PostgreSQL library NOT found. test_TGMM_parser_SQL not generated")
	endif()
ENDIF()