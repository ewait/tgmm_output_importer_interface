/*
*
* Authors: Fernando Amat
*  test_TGMM_parser_XML.cpp
*
*  Created on : March 22nd, 2015
* Author : Fernando Amat
*
* \brief Testing suite for XML importer from TGMM output to CSV
* 
*/

#include <string>
#include "TGMMparserSQL.h"



int main(int argc, const char** argv)
{
	std::cout << "test TGMM parser SQL running..." << std::endl;

	//inputs
	//TODO: autodetect data folder
	std::string filename ("C:/Users/Fernando/cppProjects/TGMMoutputParser/test/data/databaseInfo.txt");
	std::string projectName("12-08-28 Drosophila TGMM TM1000 Neuroblast");
	int TM = 3;
	
	if (argc > 1)
		filename = std::string(argv[1]);

	if (argc > 2)
		projectName = std::string(argv[2]);

	if (argc > 3)
		TM = atoi(argv[3]);


	//initialize classes
	TGMM_parse_SQL parser;

	//setup info
	int err = parser.read_DBinfo_from_txt_file(filename, projectName);
	if (err != 0)
		return err;
	
	//read XML file
	err = parser.read_from_TGMM_output(TM);
	if (err != 0)
		return err;

	//write output as CSV file
	std::string filenameCSV(filename + ".csv");
	err = parser.write_to_xpiwit_CSV(filenameCSV);
	if (err != 0)
		return err;

	//make copy
	TGMM_parse_SQL parserA (parser);

	//read CSV file	
	err = parser.read_from_xpiwit_CSV(filenameCSV);
	if (err != 0)
		return err;

	//check that both parsers contain the same information
	if (parser != parserA)
	{
		std::cout << "ERROR: parsers are not equal" << std::endl;
		return 10;
	}

	std::cout << "test TGMM parser XML passed" << std::endl;
	return 0;
}