cmake_minimum_required (VERSION 2.8)


#contains files to implement interface to read local XML files


#interface
file(GLOB FILE_SRCS *.cpp)
file(GLOB FILE_HDRS *.h)


#XML files
file(GLOB XML_SRCS parse_XML_output/*.cpp)
file(GLOB XML_HDRS parse_XML_output/*.h)

#Post-gre SQL query for CATMAID
add_subdirectory(parse_SQL_CATMAID)
include_directories(${PostgreSQL_INCLUDE_DIRS})
link_directories(${PostgreSQL_LIBRARY_DIRS})

#add flags for the C++11 
if(CMAKE_COMPILER_IS_GNUCXX) 
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

elseif ("${CMAKE_CXX_COMPILER_ID}" MATCHES "Clang")
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -stdlib=libc++ -std=c++11")
endif()

IF( NOT TARGET TGMM_parser_lib)
	ADD_LIBRARY(TGMM_parser_lib ${FILE_SRCS} ${FILE_HDRS} ${XML_SRCS} ${XML_HDRS} ${FILE_POSTGRESQL_SRCS} ${FILE_POSTGRESQL_HDRS})

	if( PostgreSQL_FOUND )
		TARGET_LINK_LIBRARIES(TGMM_parser_lib ${PostgreSQL_LIBRARY})	
	ENDIF()
ENDIF()
